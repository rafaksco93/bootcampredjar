import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { User } from '../users/models/user.models';

@Injectable({
  providedIn: 'root'
})
export class ComunicacionService {
  $user: Subject<User> = new Subject();
  constructor() { }

  emitTitle(user: User) {
    this.$user.next(user);
  }
}
