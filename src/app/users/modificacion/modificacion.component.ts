import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../user.service';

@Component({
  selector: 'app-modificacion',
  templateUrl: './modificacion.component.html',
  styleUrls: ['./modificacion.component.css']
})
export class ModificacionComponent implements OnInit {

  form: FormGroup = new FormGroup({
    id: new FormControl(),
    name: new FormControl(),
    username: new FormControl(),
    phone: new FormControl(),
  })
  id!: number;

  constructor(private _route: ActivatedRoute,
    private _service: UserService,
    private _router: Router) { }

  ngOnInit(): void {
    this.id = this._route.snapshot.params["id"];
    console.log(this.id);
    if (Number(this.id)) {
      this._service.getUserById(Number(this.id)).subscribe({
        next: (us) => {
          this.form.controls.id.patchValue(us.id);
          this.form.controls.id.disable();
          this.form.controls.name.patchValue(us.name);
          this.form.controls.username.patchValue(us.username);
          this.form.controls.phone.patchValue(us.phone);
        },
        error: (err) => alert("hubo un problema al obtener el usuario"),
      });
    } else {
      alert("Id invalido");
    }
  }

  modificarUsuario() {
    let user = this.form.value;
    this._service.putUser(this.id, user).subscribe({
      next: () => {
        alert("TODO OK");
      },
      error: (err) => alert("hubo un problema al obtener el usuario"),
      complete: () => this._router.navigate(["/users"])
    });
  }

}
