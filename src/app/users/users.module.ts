import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { ListComponent } from './list/list.component';
import { HttpClientModule } from '@angular/common/http';
import { UserService } from './user.service';
import { DetailsComponent } from './details/details.component';
import { UtilsModule } from '../utils/utils.module';
import { AltaComponent } from './alta/alta.component';
import { ModificacionComponent } from './modificacion/modificacion.component';
import { Componente1Component } from './componente1/componente1.component';


@NgModule({
  declarations: [
    ListComponent,
    DetailsComponent,
    AltaComponent,
    ModificacionComponent,
    Componente1Component
  ],
  imports: [
    CommonModule,
    UsersRoutingModule,
    HttpClientModule,
    UtilsModule
  ],
  providers: [
    UserService
  ],
  exports:[
    Componente1Component
  ]
})
export class UsersModule { }
