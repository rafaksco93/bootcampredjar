import { Component, Input, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { Observable, Subject, Subscription } from 'rxjs';
import { ComunicacionService } from 'src/app/services/comunicacion.service';
import { User } from '../models/user.models';
import { UserService } from '../user.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit, OnDestroy{
  // user : User |undefined;
  observable: Subject<User> = new Subject(); 
  @Input() user: User = {} as User;
  @Output() salida: EventEmitter<any> = new EventEmitter<any>()
  constructor(
    private _userService: UserService,
    private _comunicationService: ComunicacionService
  ) { }

  ngOnInit(): void {
    this.observable = this._comunicationService.$user;
    
    this.observable.subscribe({
      next: (us) => this.user = us
    });
    // this._userService.getUserById(2).subscribe({
    //   next: (x) => {
    //     this.user = x
    //     console.log(this.user)
    //   }
    // })


  }
  enviartexto(text: any) {
    this.salida.emit(text.target.value)
  }

  ngOnDestroy() {
    this.observable.unsubscribe();
  }
}
