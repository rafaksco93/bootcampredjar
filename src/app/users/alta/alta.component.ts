import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { User } from '../models/user.models';
import { UserService } from '../user.service';

@Component({
  selector: 'app-alta',
  templateUrl: './alta.component.html',
  styleUrls: ['./alta.component.css']
})
export class AltaComponent implements OnInit {
  constructor(private _userService: UserService,
    private _formBuilder: FormBuilder) { }
  
  get id() {
    return this.form.controls.id;
  }  
  
  get name() {
    return this.form.controls.name;
  } 
  
  get username() {
    return this.form.controls.username;
  }

  get phone() {
    return this.form.controls.phone;
  } 

  form: FormGroup = new FormGroup({
    id: new FormControl(),
    name: new FormControl(),
    username: new FormControl(),
    phone: new FormControl(),
  })

  // form2: FormGroup = this._formBuilder.group({
  //   id: [null, Validators.required],
  //   name: [null, Validators.required, Validators.minLength(3)],
  //   username: [null, Validators.required],
  //   phone: [null, [Validators.required, Validators.minLength(5)]],
  // })

  user: User = { } as User;

  ngOnInit(): void {
  }

  nuevoUsuario() {
    if (this.form.valid) {
      this.user.id = this.id.value;
      this.user.name = this.name.value;
      this.user.username = this.username.value;
      this.user.phone = this.phone.value
      this._userService.postUser(this.user).subscribe({
        next: (us) => {
          console.log(us)
        },
        error: err => console.log(err)
      })
    } else {
      console.log('ERROR, FALTAN CAMPOS POR COMPLETAR')
    }
  }
}
