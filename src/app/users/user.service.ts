import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';
import { User } from './models/user.models';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private _httpClient: HttpClient) { }

  getUsers(): Observable<User[]> {
    return this._httpClient.get<User[]>('https://jsonplaceholder.typicode.com/users')
  }

  getUsersPromise(): Promise<any> {
    return this._httpClient.get('https://jsonplaceholder.typicode.com/users').toPromise();
  }

  getUserById(id: number): Observable<User> {
    return this._httpClient.get<User>(`https://jsonplaceholder.typicode.com/users/${id}`)
  }
  
  deleteUserById(id: number): Observable<User> {
    return this._httpClient.delete<User>(`https://jsonplaceholder.typicode.com/users/${id}`)
  }

  postUser(user: User): Observable<User> {
    return this._httpClient.post<User>('https://jsonplaceholder.typicode.com/users', user)
  }

  putUser(id: number, user: User): Observable<User> {
    return this._httpClient.put<User>(`https://jsonplaceholder.typicode.com/users/${id}`, user)
  }
}
