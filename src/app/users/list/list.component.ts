import { Component, EventEmitter, Input, OnInit } from '@angular/core';
import { UserService } from '../user.service'
import { User } from '../models/user.models';
import { Router } from '@angular/router';
import { ComunicacionService } from 'src/app/services/comunicacion.service';

@Component({
  selector: 'app-lista',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  @Input() entrada: string = '';
  eventEmmiter = new EventEmitter();
  displayedColumns: string[] = ['id', 'name', 'phone', 'username', 'accion'];
  listUsers: User[] = [];
  idUser: number | undefined

  userEmiter: User = {} as User;

  constructor(
    private _userService: UserService,
    private _route: Router,
    private _comunicationService: ComunicacionService) { }

  ngOnInit(): void {
    let start = 1;
    this.getUsers();
  }

  getUsers() {
    this.eventEmmiter.subscribe({
      next: (x: any) => console.log(x)
    });
    this._userService.getUsers().subscribe({
      next: (x: User[]) => {
        this.listUsers = x;
        console.log(this.listUsers as User[])
      }
    });
  }

  deleteUser(id: number) {
    this._userService.deleteUserById(id).subscribe({
      next: () => {
        alert("borrado con exito");
        this.listUsers = this.listUsers.filter(x => x.id != id);
        //this.getUsers(); esto es lo que esta bien
      },
      error: () => alert("hubo un error al eliminar")
    });
  }

  goToEditUser(id: number) {
    this._route.navigate([`/users/modificacion/${id}`]);
  }

  verDetalles(user: User) {
    this._comunicationService.emitTitle(user);
  }
}
